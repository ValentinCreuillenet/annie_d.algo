/**
 * Exercice sur les suites
 * 
 * 1-Afficher la suite des nombres  entiers (0>1000)
 * 
 * 2-Afficher les 50 premiers nombres de la suite de Fibonacci
 * 
 * 3-Afficher les 50 premier nombres entiers paires puis impaires
 * 
 * 4-Extra : Afficher les nombre paire de la suite de Fibonacci
 */

//Affichage de la suite des nombres entier de 0 à 100
console.log("Les entiers de 0 à 100")
for(let i=0;i<=100;i++){
    console.log(i);
}

//Affichage des 50 premiers nombres de la suite de Fibonacci

var f = 1; //Le nombre actuel dans la suite de Fibonacci
var e = 0; //Le nombre pré cedent dans la suite de Fibonacci
console.log("Les 50 premiers nombres de la suite de Fibonacci :");
for(i=1;i<=50;i++){
    console.log(f);
    f+=e;
    e=f-e;
}

//Affichage des nombres paires parmis les premiers de la suite de Fibonacci

f = 1;
e = 0;
console.log("Nombre paires parmis les 50 premiers de la suite de Fibonacci :")
for(i=1;i<=50;i++){
    if(f%2==0)console.log(f);
    f+=e;
    e=f-e;
}

//Affichage des nombres impaires parmis les premiers de la suite de Fibonacci

f = 1;
e = 0;
console.log("Nombre impaires parmis les 50 premiers de la suite de Fibonacci :")
for(i=1;i<=50;i++){
    if(f%2!=0)console.log(f);
    f=f+e;
    e=f-e;
}

//Affichage des 50 nombres entiers paires
console.log("Les 50 premiers entier paires :");
for(i=1;i<=100;i++){
    if(i%2==0)console.log(i);
}

//Affichage des 50 premiers nombres entiers impaires
console.log("Les 50 premiers entiers impaires :");
for(i=1;i<=100;i++){
    if(i%2!=0)console.log(i);
}

/**
 * Exercice sue les palindromes
 * 
 * Vérifier si le mot est un palindrome ou pas
 * 
 */

var palindrome = "Beweb";
var palintest ="";


for(i=palindrome.length-1;i>=0;i--){
    palintest+=palindrome[i];
}

if(palindrome.toLowerCase()==palintest.toLowerCase()){
    console.log(palindrome+" est un palindrome");
}else{
    console.log(palindrome+" n'est pas un palindrome");
}